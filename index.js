/*jshint esversion: 8 */

// npm modules (dependencies)
const http = require('http');
const publicIp = require('public-ip');
const xpath = require('xpath');
const dom = require('xmldom').DOMParser;
const domToStr = require('xmldom').XMLSerializer;
const puppeteer = require('puppeteer');
const fetch = require('isomorphic-fetch');
const {convert} = require('exchange-rates-api');
const htmlParser = require('node-html-parser');

const jsdom = require("jsdom");
const { JSDOM } = jsdom;

// API communication metadata
const port = 8011;
const host = 'localhost';

// globals declarations
let scrapingUnitId;
let domDoc;
let heartBeatIntervalValue;                             // częstotliwość z jaką wykonje się request hearBeat
let heartBeatInterval;                                  // przypisany interval, który wywołuje funkcje heartBeat
var currectprice;
var scrapingid;
var tasksSize;
var lista = [];
var Str;


function replaceAll(str, search, replacement) {
    return str.split(search).join(replacement);
}

function getElementsByXpath(path) {
    var result = [];
    var nodesSnapshot = xpath.evaluate(path, domDoc, null, xpath.XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null );


    var r = xpath.evaluate(
        path,                       // xpathExpression
        domDoc,                        // contextNode
        null,                       // namespaceResolver
        xpath.XPathResult.ANY_TYPE, // resultType
        null                        // result
    );
     
    node = r.iterateNext();
    while (node) {
        console.log(node.localName + ": " + node.firstChild.data);
        console.log("Node: " + node.toString());
     
        node = r.iterateNext();
    }



    // for ( var i=0 ; i < nodesSnapshot.snapshotLength; i++ ){
    //   result.push( nodesSnapshot.snapshotItem(i) );
    // }
    // return result;

    return "nic";
}

// ------ uniwersalna funkcja do wykonywania requestów api -----------------------------------------------------------------
//path - adres requesta
//data - dane przesyłane do api
//fun - funkcja wykonywana po udanym wykonaniu requesta i uzyskaniu odpowiedzi od serwera
function apiRequest(path, data, fun) {

    var allData = "";

    const options = {
        hostname: host,
        port: port,
        path: path,
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': data.length
        }
    };


    const req = http.request(options, res => {
        res.on('data', d => {
            allData += d;
        });
        res.on('end', () => {
            fun(allData);
            allData = "";
        });
    });

    req.on('error', error => {
        console.error(error);
    });
    req.write(data);
    req.end();

}


// ------ funkcja do requesta init w api -----------------------------------------------------------------
async function init() {
    //await jest potrzebne aby poczekać na zwrot adresu ip
    ip = await publicIp.v4();
    //dane wysyłane requestem do api
    let data = JSON.stringify({
        "IP": ip
    });

    apiRequest('/api/scraping-unit/init', data, init2);
}

function init2(d) {
    const obj = JSON.parse(d);

    heartBeatIntervalValue = obj.heartBeatIntervalInSec;
    scrapingUnitId = obj.scrapingUnitId;
    heartBeatInterval = setInterval(function() {
        heartbeat()
    }, heartBeatIntervalValue * 1000);

    //po udanej inicjacji uruchamia sie jobRequest
    heartbeat()
    jobRequest();
}


// ------ funkcja do requesta heartbeat w api -----------------------------------------------------------------
function heartbeat() {
    //usunięcie starego interval, w razie zmiany czestotliwości heartBeat
    clearInterval(heartBeatInterval);
    //dane wysyłane requestem do api
    let data = JSON.stringify({
        "scrapingUnitId": scrapingUnitId
    });

    apiRequest('/api/scraping-unit/heartbeat', data, heartbeat2);
}

function heartbeat2(d) {
    const obj = JSON.parse(d);
    heartBeatIntervalValue = obj.heartBeatIntervalInSec;

    heartBeatInterval = setInterval(function() {
        heartbeat()
    }, heartBeatIntervalValue * 1000);
}

// ------ funkcja do requesta jobRequest w api -----------------------------------------------------------------
function jobRequest() {
    //dane wysyłane requestem do api
    scrapingid = JSON.stringify({
        "scrapingUnitId": scrapingUnitId
    });
    lista=[];
    apiRequest('/api/scraping-unit/job-request', scrapingid, jobRequest2);
    scrapingid = scrapingUnitId;
}

async function jobRequest2(d) {

    console.log("");
    console.log("");
    console.log("--- ETAP: job-request otrzymano odpowiedź od serwera");
    console.log("");

    const tasks = JSON.parse(d);
    tasksSize = Object.keys(tasks).length;
    resultList = new Array;

    console.log("DANE: poniżej wylistowane zadania dla SU");

    for (var key in tasks) {
        if (tasks.hasOwnProperty(key)) {


            url = key;

            console.log("");
            console.log("url: " + url);

            algorithms = tasks[key]["algorithms"];
            resultList[key] = url;
            resultList[key] = algorithms;
            resultList[key] = await scraping(url, algorithms);

            // console.log("ZADANIE");
            // console.log("algorithms: " + JSON.stringify(algorithms));

        }
    }
    var output = {
        "scrapingUnitId": scrapingid,
        "results": lista
    };
    jobResult(output)
}

// ------ funkcja do requesta jobResult w api -----------------------------------------------------------------
function jobResult(resultList) {
	
    let data = JSON.stringify(resultList);
    // console.log(resultList)
    apiRequest('/api/scraping-unit/job-result', data, jobRequest3);
}

function jobRequest3(d) {
    //po wykonaniu jobResult czyli przekazaniu danych ze scrapingu do api, znów zaczyna scrapowac
    jobRequest();
}

// ------ funkcja do załadowania strony(bez wykonania js) i utworzenie struktury dom z tej strony -----------------------------------------------------------------
async function loadDom(url) {
    const options = {
        url: url,
        headers: {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36'
        }
    };

    const response = await fetch(url, options);
    var text = await response.text();

    text = replaceAll(text, "&nbsp;", "");
    text = replaceAll(text, "&iacute;", "");
    text = replaceAll(text, "&oacute;", "");
    text = replaceAll(text, "&acute;", "");
    text = replaceAll(text, "&eacute;", "");
    text = replaceAll(text, "&aacute;", "");
    text = replaceAll(text, "&copy;", "");
    text = replaceAll(text, "&times;", "");

    

    text = replaceAll(text, "<span\">", "<span>");
    text = replaceAll(text, "async", "");



    text = replaceAll(text, "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">", "");


    text = replaceAll(text, "xmlns=\"http://www.w3.org/1999/xhtml\"", "");

    




    var debugOptions = {locator:{},errorHandler:{warning:function(w){},error:function(w){},fatalError:function(w){console.log("BŁĄD KRYTYCZNY PARSERA KODU HTML: " + w);}}};

    console.log("     zaraz parsowanie HTML");
    domDoc = new dom(debugOptions).parseFromString(text);

    
    // domDoc = htmlParser.parse(text);

    // domDoc = new JSDOM(text).window.document;

    // if (url.includes("eurohosta")) {

        // zwrotna konwersja DOM do string i wyswietlenie
        // var s = new domToStr();
        // console.log(s.serializeToString(domDoc));

        // wyswietlanie sparsowanego dom'a a konrketniej jeden element z ID
        // // var x = domDoc.getElementById("product-price-1886");
        // // console.log(x.childNodes[1]);
        // // console.log(x.childNodes[1].attributes);
        // // console.log(x.childNodes[1].attributes[0]);
    // }


}

// ------ funkcja do załadowania strony(z wykonaniem js) i utworzenie struktury dom z tej strony -----------------------------------------------------------------
async function loadDomFull(url) {
    const browser = await puppeteer.launch({
        headless: true,
    });
    const page = await browser.newPage();
    await page.setRequestInterception(true);

    page.on('request', (req) => {
        if (req.resourceType() == 'font' || req.resourceType() == 'image') {
            req.abort();
        } else {
            req.continue();
        }
    });

    await page.goto(url);
    let bodyHTML = await page.evaluate(() => document.body.innerHTML);
    domDoc = new dom({errorHandler:{warning:function(w){},error:function(w){},fatalError:function(w){}}}).parseFromString(bodyHTML);
    browser.close();
}

// ------ funkcja scrapujac -----------------------------------------------------------------
// url - adres strony do załadowania i zescrapowania
// algorithms - lista algorytmów za pomcą któych bedzie scrapowac
async function scraping(url, algorithms) {




    var loadedFull = false;
    //pierwsze załadownie strony bez js, aby przyspieszyc dzialanie



    await loadDom(url);



        //petla przegląda algorytmy i dla każdego wyciąga cenę i tworzy obiekt, który zostanie przekazany do api
        
        console.log("     ilość algorytmów: " + algorithms.length);
        for (var i = 0; i < algorithms.length; i++) {
            algorithm = algorithms[i];

            console.log("     xpath: " + algorithm.htmlPriceSelector);

            // console.log("Wczytujemy cene z elementu o xpath:");
            // console.log(algorithm.htmlPriceSelector);
            
            var title;
            try {
                title = xpath.select(algorithm.htmlPriceSelector, domDoc);
            }
            catch(err) {
                // console.log("Problem z parsowaniem XPath:");
                // console.log(algorithm.htmlPriceSelector);
                console.log("     ERROR: błędny xpath");
            }

            // var arr = getElementsByXpath(algorithm.htmlPriceSelector);
            // console.log(arr);

            if(title!= null && title.length > 0){
                console.log("     znaleziono " + title.length + " wystąpień");
                if(title[0]!= null){
                    // console.log("pierwsze wystąpienie:");
                    // console.log("id: " + title[0].id);
                    // console.log("classes: " + title[0].className);
                    // console.log("textContent: " + title[0].textContent);
                }else{
                    console.log("     ERROR: pierwsze znalezisko <elem> jest NULL");
                }
            }else{
                console.log("     ERROR: nie znaleziono <elem> po xpath");
            }


            if (title == null || title[0] == null) {
                Str = {
                    "url": url,
                    "algorithms": [{
                        "id": algorithm.id,
                        "time": new Date().toISOString().replace('T', ' ').substr(0, 19),
                        "status": 0,
                        "observedProductIds": algorithm.observedProductIds
                    }]
                }
            } else {

                if (algorithm.priceFormatDetails != null) {
                    var formatDetailsinteger = xpath.select(algorithm.priceFormatDetails.integerPart, domDoc);
                    var formatDetailsfractional = xpath.select(algorithm.priceFormatDetails.fractionalPart, domDoc);
                    console.log("     tryb odczytywania przez priceFormatDetails");
                }
                //kiedy jest priceformatdetails
                if (algorithm.priceFormatDetails != null && formatDetailsinteger.toString().length > 0 && formatDetailsfractional.toString().length > 0) {
                    integer = formatDetailsinteger[0].textContent;
                    integer = integer.replace("&nbsp;", "").trim();
                    fractional = formatDetailsfractional[0].textContent;
                    fractional = fractional.replace("&nbsp;", "").trim();
                    price = integer + "." + fractional
                    price = parseFloat(price)


                } else if (algorithm.priceFormatDetails != null && !loadedFull) { //sprawdza czy strona juz byla zaladowana, w przypadku wielu algorytmów może być tak, że się nie uda odczytac a strona już jest doładowana z js
                    await loadDomFull(url);
                    loadedFull = true;
                    var formatDetailsinteger = xpath.select(algorithm.priceFormatDetails.integerPart, domDoc);
                    var formatDetailsfractional = xpath.select(algorithm.priceFormatDetails.fractionalPart, domDoc)

                    integer = formatDetailsinteger[0].textContent;
                    integer = integer.replace("&nbsp;", "").trim();
                    fractional = formatDetailsfractional[0].textContent;
                    fractional = fractional.replace("&nbsp;", "").trim();
                    price = integer + "." + fractional
                    price = parseFloat(price)
                    price = price.toString()

                }
                //sprawdza czy udalo sie pobrac cene jak nie to ładuje strone z js, chyba ze już była załadowana
                if (title.toString().length > 0) {
                    price = title[0].textContent;
                    price = price.replace("&nbsp;", "").trim();
                    console.log("     surowa cena w string: " + price);

                } else if (!loadedFull) { //sprawdza czy strona juz byla zaladowana, w przypadku wielu algorytmów może być tak, że się nie uda odczytac a strona już jest doładowana z js
                    await loadDomFull(url);
                    loadedFull = true;
                    var title = xpath.select(algorithm.htmlPriceSelector, domDoc);
                    if (title[0] == null) {
                        Str = {
                            "url": url,
                            "algorithms": [{
                                "id": algorithm.id,
                                "time": new Date().toISOString().replace('T', ' ').substr(0, 19),
                                "status": 0,
                                "observedProductIds": algorithm.observedProductIds
                            }]
                        }
                    }
                } else {
                    price = title[0].textContent;
                    price = price.replace("&nbsp;", "").trim();

                }


                if (algorithm.priceCurrency == 'EUR') //zamienia EUR na PLN
                {
                    
                    let data = new Date();
                    price = price.replace(/ /, '');
                    price = price.replace(/,/, '.');
                    price = price.replace('€', '');
                    
                    price = parseFloat(price);

                    price = await convert(price, 'EUR', 'PLN', data);
                    price = Math.round((price + Number.EPSILON) * 100) / 100;
                    price = price.toString();

                    console.log("     cena w EUR");
                };
                isthepriceisreal(price); //funkcja do sprawdziania poprawności ceny

                function isNumber(str) {
                    return !isNaN(str);
                }

                function cutPriceStrFromLeftAndRight(str) {

                    var leftStart = 0;
                    var len = str.length;
                    var char;

                    for (var i=0; i<str.length; i++) {
                        char = str.substr(i, 1);
                        if (isNumber(char)) {
                            leftStart = i;
                            break;
                        }
                    }

                    for (var j=str.length-1; j>=0; j--) {
                        char = str.substr(j, 1);
                        if (isNumber(char)) {
                            len = j+1;
                            break;
                        }
                    }

                    return str.substr(leftStart, len);
                }

                function isthepriceisreal(price) {
                    var isOnePriceLikeNumberInside = false;
                    var regex = /\d(\d|\s)+(((\.|\,)\d\d)?)/g;
                    var foundPhrasesArr = price.match(regex);
                    if (foundPhrasesArr != null && foundPhrasesArr.length > 0 && foundPhrasesArr.length < 15) {
                        isOnePriceLikeNumberInside = true;
                    }
                    price = price.replace(',', '.');
                    price = price.replace(/ /, '');
                    price = price.replace("zł", '');
                    price = price.replace("zl", '');
                    price = price.replace("pln", '');
                    price = price.replace(",-", '');
                    price = price.replace("cena:", '');
                    price = price.replace("cena", '');

                    price = cutPriceStrFromLeftAndRight(price);

                    console.log("     cena po obróbce: " + price);

                    currectprice = parseFloat(price);


                    if (isOnePriceLikeNumberInside == true) {
                        status = 1
                    } else {
                        console.log("     ERROR: niepoprawny format ceny");
                        status = 0
                    }
                    return currectprice
                }
                //tworzy obiekt o odpowiedniej strukturze dla api
                Str = {
                    "url": url,
                    "algorithms": [{
                        "id": algorithm.id,
                        "price": currectprice,
                        "time": new Date().toISOString().replace('T', ' ').substr(0, 19),
                        "status": status,
                        "observedProductIds": algorithm.observedProductIds
                    }]
                };

                console.log("     odczytana cena: " + currectprice);
            }
        }
        // console.log(Str)
        lista.push(Str);
    
}

init();
//jobRequest();
//jobResult();

//  algo = [{id:3,htmlPriceSelector:"//div[@class='productShop-sp__root-2EZ']",priceFormatDetails:{integerPart:"//div[@class='productShop-sp__integer-2tE']",fractionalPart:"//div[@class='productShop-sp__fraction-3RX']"}, priceCurrency:"PLN"}];
//  url="https://www.neonet.pl/sluchawki/xiaomi-bt-xiaomi-mi-true-wireless-biale.html";
//  test=scraping(url,algo);
//  // console.log("www");
//  // console.log('to jest test')
//  // console.log(test);


// var title = xpath.select("//div", dom3);
// // console.log(title);
// // console.log(dom3.toString());